# SensorThink Automation Framework
Hello and welcome to SensorThink Automation Framework repository. Please take a moment to look over this readme and familiarize yourself with the framework. 

## Overview
The SensorThink automation framework is built using a design pattern known as Page Object Model (POM). In POM we create a separate class to represent each page of the application being tested. Inside each class we write methods for interacting with the various elements on a page. Our test cases can use these methods over an over again without having to rewrite code. The key benefits to this strategy is that your code is easier to read, reusable, and easier to maintain.

## Stack

 - Selenium
 - TestNG
 - Maven
 - Allure

## Getting started

 1. Clone the repository
 ```git clone https://jhartley1981@bitbucket.org/jhartley1981/qa-automation.git```
 2. Import the project into your preferred IDE
 3. Prepare the project to run locally
 ```mvn clean install```

## Creating page classes

Page classes live in the 'src/main/java/pages' directory. Each page class can be extended off the 'BasePage' class. This will give access to a base set of methods. These base methods handle very common and simple tasks such as assert, click, read, wait, and write. 


## Creating test classes
Test classes live in the 'src/test/java/tests' directory. Each test class must be extended off the 'BaseTest' class. This class handles setting up the test driver to supply selenium grid with the information it needs to execute the test. Inside the test class you will call methods from the page object in order to execute the test. For example:
```java 
public class LoginTests extends BaseTest {  
    @Test (priority = 0)  
    @Severity(SeverityLevel.BLOCKER)  
    @Description("Verify valid users are able to login and invlad users are not.")  
    @Story("Valid username and password login test")  
    public void loginTestHappyPath () {  
  
        //*************PAGE INSTANTIATIONS*************  
  LoginPage loginPage = new LoginPage(driver);  
  WavesPage wavesPage = new WavesPage(driver);  
  
  //*************PAGE METHODS********************  
  
 // Go to site  
 loginPage.goToLoginPage();  
 // Login to site  
 loginPage.loginToApp("wesadmin@beauty699.nordstrom.com", "6Py!ZcH@6vPz");  
 // Click submit button  
 loginPage.goToWavesPage();  
 // Verify login  
 wavesPage.validatePage();  
  }  
}
```