package tests;

import io.qameta.allure.*;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.WavesPage;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
@Epic("WES_LOGIN_AUTOMATION")
@Feature("Login Tests")
public class LoginTests extends BaseTest {
    @Test (priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify valid users are able to login and invlad users are not.")
    @Story("Valid username and password login test")
    public void loginTestHappyPath () {

        //*************PAGE INSTANTIATIONS*************
        LoginPage loginPage = new LoginPage(driver);
        WavesPage wavesPage = new WavesPage(driver);

        //*************PAGE METHODS********************

        // Go to site
        loginPage.goToLoginPage();
        // Login to site
        loginPage.loginToApp("wesadmin@beauty699.nordstrom.com", "6Py!ZcH@6vPz");
        // Click submit button
        loginPage.clickLoginButton();
        // Verify login
        wavesPage.validatePage();

    }


}
