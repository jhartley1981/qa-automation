package tests;

import utils.Listeners.TestListener;
import utils.SetupTestDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import utils.Log;

import java.net.MalformedURLException;

public class BaseTest {
    public WebDriver driver;
    public WebDriverWait wait;

    public WebDriver getDriver() {
        return this.driver;
    }


    @BeforeClass(alwaysRun = true)
    @Parameters({"browser", "node"})

    public void setUp(String browser, String node) throws MalformedURLException {
        //Write a Log when tests is starting
        Log.startLog("Test is starting!");

        //Create a driver. All test classes use this.
        SetupTestDriver setupTestDriver = new SetupTestDriver(browser, node);
        driver = setupTestDriver.getDriver();

        //Create a wait. All test classes use this.
        wait = new WebDriverWait(driver,15);

        //Maximize Window
        driver.manage().window().maximize();


    }

    @AfterClass(description = "Class Level Teardown!")
    public void teardown () {
        //Write a Log when tests are ending
        Log.endLog("Test is ending!");
        driver.quit();
    }
}
