package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginPage extends BasePage{

    //*********Constructor*********
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    //*********Page Variables*********
    String baseURL = "https://qa.sensorthink.com/wes/#/login";

    //*********Web Elements*********
    By usernameBy = By.id("username");
    By passwordBy = By.id("password");
    By loginButtonBy = By.className("login_btn");


    //*********Page Methods*********
    @Step("Open Login page")
    public LoginPage goToLoginPage(){
        driver.get(baseURL);
        return this;
    }
    @Step("Enter username and password into corresponding fields")
    public LoginPage loginToApp (String username, String password){
        //Enter Username(Email)
        writeText(usernameBy,username);
        //Enter Password
        writeText(passwordBy, password);

        return this;
    }
    @Step("Click login button")
    public LoginPage clickLoginButton () {
        click(loginButtonBy);
        return new LoginPage(driver);
    }


}
