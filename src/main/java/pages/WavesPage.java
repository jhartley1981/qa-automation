package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WavesPage extends BasePage{

    //*********Constructor*********
    public WavesPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    By wavesLogoBy = By.className("tompkinWesLogoD");


    //*********Page Methods*********

    @Step("Validate waves page is displayed")
    public void validatePage() {
        // validate current url test
        Assert.assertTrue(driver.getCurrentUrl().contains("https://qa.sensorthink.com/wes/#/waves"));
        assertEquals(wavesLogoBy, "");
    }

}
